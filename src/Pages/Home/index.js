import React from 'react';
import { Page } from '../../common-components';
import { ContactsForm, ContactsList, ContactView } from '../../ContactsManager';

const Home = () => {
	return (
		<Page>
			<ContactsForm />
			<ContactsList />
			<ContactView />
		</Page>
	)
}

export default Home;