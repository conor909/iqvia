import React from 'react';
import { Page } from '../../common-components';

const About = () => (
	<Page>
		<h1>About Page</h1>
		<p>
			<a href='https://www.conor-ui.com'>
				www.conor-ui.com
      </a>
		</p>
	</Page>
)

export default About
