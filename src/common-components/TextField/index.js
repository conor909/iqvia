import React from 'react';
import TextField from '@material-ui/core/TextField';

export default ({
	id,
	label,
	value,
	onChange,
	onBlur = () => { }
}) => {

	function handleOnBlur(e) {
		onBlur(e.target.value)
	}

	function handleOnChange(e) {
		onChange(e.target.value)
	}

	return (
		<TextField
			id={id}
			label={label}
			className='text-input'
			value={value}
			onBlur={handleOnBlur}
			onChange={handleOnChange}
			margin="normal"
		/>
	)
}