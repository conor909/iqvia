import React from 'react';

export default () => (
	<div
		style={{
			display: 'flex',
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center'
		}}>
		{'Loading...'}
	</div>
)