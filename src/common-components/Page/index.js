import React from 'react';


function Page(props) {
	return (
		<div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
			{props.children}
		</div>
	)
}

export default Page;