import React from 'react';
import Button from '@material-ui/core/Button';

export default ({
	variant = 'contained',
	buttonType = 'primary',
	text,
	onClick
}) => {
	return (
		<Button
			variant={variant}
			color={buttonType}
			onClick={onClick}>
			{text}
		</Button>
	)
}