import React, { useState } from 'react'
import { Header } from '../common-components';
import { Home } from '../Pages';

const App = () => {

	const [isNavOpen, setIsNavOpen] = useState(false);

	return (
		<div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
			<Header toggleNav={() => setIsNavOpen(!isNavOpen)} />
			<main style={{ display: 'flex', flex: 1 }}>
				<Home />
			</main>
		</div>
	)
}

export default App;
