import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import apiMiddleware from '../store/middleware/api-middleware';
import App from '.';

const middlewares = [thunk, apiMiddleware]
const mockStore = configureStore(middlewares);

it('renders without crashing', () => {
	const div = document.createElement('div');
	const initialState = {
		contactsManager: {
			list: false,
			view: {
				contact: {
					id: null,
					name: null,
					email: null
				}
			}
		}
	};
	ReactDOM.render(
		<Provider store={mockStore(initialState)}>
			<App />
		</Provider>, div);
	ReactDOM.unmountComponentAtNode(div);
});