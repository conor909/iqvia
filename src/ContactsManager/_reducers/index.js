import {
	ADD_CONTACT_REQUEST, ADD_CONTACT_SUCCESS, ADD_CONTACT_FAILURE,
	GET_CONTACTS_REQUEST, GET_CONTACTS_SUCCESS, GET_CONTACTS_FAILURE,
	GET_CONTACT_REQUEST, GET_CONTACT_SUCCESS, GET_CONTACT_FAILURE,
	DELETE_CONTACT_REQUEST, DELETE_CONTACT_SUCCESS, DELETE_CONTACT_FAILURE,
	EDIT_CONTACT_REQUEST, EDIT_CONTACT_SUCCESS, EDIT_CONTACT_FAILURE
} from '../_types';

import { addContactRequest, addContactSuccess, addContactFailure } from './add-contact';
import { getContactsRequest, getContactsSuccess, getContactsFailure } from './get-contacts';
import { getContactRequest, getContactSuccess, getContactFailure } from './get-contact';
import { deleteContactRequest, deleteContactSuccess, deleteContactFailure } from './delete-contact';
import { editContactRequest, editContactSuccess, editContactFailure } from './edit-contact';

import { ContactsManagerInterface } from '../Interfaces';

import initialState from './initial-state';

export default (state = initialState, action) => {
	switch (action.type) {

		case GET_CONTACT_REQUEST:
			return getContactRequest(state, action);
		case GET_CONTACT_SUCCESS:
			return getContactSuccess(state, action);
		case GET_CONTACT_FAILURE:
			return getContactFailure(state, action);

		case GET_CONTACTS_REQUEST:
			return getContactsRequest(state, action);
		case GET_CONTACTS_SUCCESS:
			return getContactsSuccess(state, action);
		case GET_CONTACTS_FAILURE:
			return getContactsFailure(state, action);

		case ADD_CONTACT_REQUEST:
			return addContactRequest(state, action);
		case ADD_CONTACT_SUCCESS:
			return addContactSuccess(state, action);
		case ADD_CONTACT_FAILURE:
			return addContactFailure(state, action);

		case DELETE_CONTACT_REQUEST:
			return deleteContactRequest(state, action);
		case DELETE_CONTACT_SUCCESS:
			return deleteContactSuccess(state, action);
		case DELETE_CONTACT_FAILURE:
			return deleteContactFailure(state, action);

		case EDIT_CONTACT_REQUEST:
			return editContactRequest(state, action);
		case EDIT_CONTACT_SUCCESS:
			return editContactSuccess(state, action);
		case EDIT_CONTACT_FAILURE:
			return editContactFailure(state, action);

		default:
			return state;
	}
}

export {
	ContactsManagerInterface
}