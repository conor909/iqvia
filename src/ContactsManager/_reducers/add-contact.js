import update from 'immutability-helper';

export function addContactRequest(state, action) {
	const updatedState = update(state, {});
	return updatedState
}

export function addContactSuccess(state, action) {
	const updatedState = update(state, {
		list: { $push: [action.contact] }
	});
	return updatedState
}

export function addContactFailure(state, action) {
	const updatedState = update(state, {});
	return updatedState
}