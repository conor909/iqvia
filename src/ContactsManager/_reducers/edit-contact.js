import update from 'immutability-helper';

export function editContactRequest(state, action) {
	const updatedState = update(state, {});
	return updatedState
}

export function editContactSuccess(state, action) {
	const updatedState = update(state, {
		list: { $splice: [[action.contactIndex, 1, action.updatedContact]] }
	});
	return updatedState
}

export function editContactFailure(state, action) {
	const updatedState = update(state, {});
	return updatedState
}