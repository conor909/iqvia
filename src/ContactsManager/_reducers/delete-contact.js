import update from 'immutability-helper';

export function deleteContactRequest(state, action) {
	const updatedState = update(state, {});
	return updatedState
}

export function deleteContactSuccess(state, action) {
	const updatedState = update(state, {
		list: { $splice: [[action.contactIndex, 1]] }
	});
	return updatedState
}

export function deleteContactFailure(state, action) {
	const updatedState = update(state, {});
	return updatedState
}