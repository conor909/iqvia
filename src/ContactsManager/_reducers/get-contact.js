import update from 'immutability-helper';

export function getContactRequest(state, action) {
	const updatedState = update(state, {});
	return updatedState
}

export function getContactSuccess(state, action) {
	const updatedState = update(state, {
		view: {
			contact: { $set: action.contact }
		}
	});
	return updatedState
}

export function getContactFailure(state, action) {
	const updatedState = update(state, {});
	return updatedState
}