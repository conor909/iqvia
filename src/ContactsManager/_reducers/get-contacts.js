import update from 'immutability-helper';

export function getContactsRequest(state, action) {
	const updatedState = update(state, {});
	return updatedState
}

export function getContactsSuccess(state, action) {
	const updatedState = update(state, {
		list: { $set: action.contacts }
	});
	return updatedState
}

export function getContactsFailure(state, action) {
	const updatedState = update(state, {});
	return updatedState
}