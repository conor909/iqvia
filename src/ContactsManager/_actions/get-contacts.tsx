import { API_REQUEST } from '../../store/middleware/api-middleware/_types';
import { GET_CONTACTS_REQUEST, GET_CONTACTS_SUCCESS, GET_CONTACTS_FAILURE } from '../_types';
import { ApiRequestInterface } from '../../store/middleware/api-middleware/index';
import { ContactInterface } from '../Interfaces';

export default function getContacts(): ApiRequestInterface {
	return {
		type: API_REQUEST,
		url: '/contacts',
		method: 'get',
		passToActions: {
			request: getContactsRequest,
			success: getContactsSuccess,
			failure: getContactsFailure
		}
	}
}

export function getContactsRequest(): InterfaceGetContactsRequest {
	return {
		type: GET_CONTACTS_REQUEST
	}
}

export function getContactsSuccess(data: ContactInterface[]): InterfaceGetContactsSuccess {
	return {
		type: GET_CONTACTS_SUCCESS,
		contacts: data
	}
}

export function getContactsFailure(data: object): InterfaceGetContactsFailure {
	return {
		type: GET_CONTACTS_FAILURE,
		data
	}
}

interface InterfaceGetContactsRequest {
	type: string;
}

interface InterfaceGetContactsSuccess {
	type: string;
	contacts: ContactInterface[];
}

interface InterfaceGetContactsFailure {
	type: string;
	data: object;
}