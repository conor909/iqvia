import { API_REQUEST } from '../../store/middleware/api-middleware/_types';
import { ApiRequestInterface } from '../../store/middleware/api-middleware';
import { DELETE_CONTACT_REQUEST, DELETE_CONTACT_SUCCESS, DELETE_CONTACT_FAILURE } from '../_types';
import { StoreInterface } from '../../store/Interfaces';
import { ContactInterface } from '../Interfaces';

export default function deleteContact(contactId: string): ApiRequestInterface {
	return {
		type: API_REQUEST,
		url: '/delete-contact',
		data: { contactId },
		method: 'post',
		passToActions: {
			request: deleteContactRequest,
			success: deleteContactSuccess,
			failure: deleteContactFailure
		}
	}
}

export function deleteContactRequest(): InterfaceDeleteContactRequest {
	return {
		type: DELETE_CONTACT_REQUEST
	}
}

export function deleteContactSuccess(data: string) {
	return (dispatch: any, getStore: () => StoreInterface) => {
		const targetId = data;
		dispatch({
			type: DELETE_CONTACT_SUCCESS,
			contactIndex: getStore().contactsManager.list.findIndex((contact: ContactInterface) => (contact.id === targetId))
		})
	}
}

export function deleteContactFailure(response: object): InterfaceDeleteContactFailure {
	return {
		type: DELETE_CONTACT_FAILURE,
		response
	}
}

interface InterfaceDeleteContactRequest {
	type: string;
}

interface InterfaceDeleteContactFailure {
	type: string;
	response: object;
}