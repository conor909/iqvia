import { API_REQUEST } from '../../store/middleware/api-middleware/_types';
import getContacts, { getContactsRequest, getContactsSuccess, getContactsFailure } from './get-contacts';
import { GET_CONTACTS_REQUEST, GET_CONTACTS_SUCCESS, GET_CONTACTS_FAILURE } from '../_types';

describe('Get Contacts Actions', () => {
	it('should create an action to get all contacts', () => {
		const action = getContacts();
		expect(action).toMatchObject({
			type: API_REQUEST,
			url: '/contacts',
			method: 'get',
			passToActions: {
				request: expect.any(Function),
				success: expect.any(Function),
				failure: expect.any(Function)
			}
		})
	});

	it('should create a request action', () => {
		const action = getContactsRequest();
		expect(action).toMatchObject({
			type: GET_CONTACTS_REQUEST
		});
	})

	it('should create a success action', () => {
		const action = getContactsSuccess([{ name: 'John Smith' }]);
		expect(action).toMatchObject({
			type: GET_CONTACTS_SUCCESS,
			contacts: [{ name: 'John Smith' }]
		});
	})

	it('should create a failure action', () => {
		const action = getContactsFailure();
		expect(action).toMatchObject({
			type: GET_CONTACTS_FAILURE
		});
	})

});