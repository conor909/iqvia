import { API_REQUEST } from '../../store/middleware/api-middleware/_types';
import { GET_CONTACT_REQUEST, GET_CONTACT_SUCCESS, GET_CONTACT_FAILURE } from '../_types';
import { ApiRequestInterface } from '../../store/middleware/api-middleware/index';
import { ContactInterface } from '../Interfaces';

export default function getContact(contactId: string): ApiRequestInterface {
	return {
		type: API_REQUEST,
		url: '/contact',
		method: 'get',
		params: {
			id: contactId
		},
		passToActions: {
			request: getContactRequest,
			success: getContactSuccess,
			failure: getContactFailure
		}
	}
}

export function getContactRequest(): InterfaceGetContactRequest {
	return {
		type: GET_CONTACT_REQUEST
	}
}

export function getContactSuccess(data: ContactInterface): InterfaceGetContactSuccess {
	return {
		type: GET_CONTACT_SUCCESS,
		contact: data
	}
}

export function getContactFailure(data: object): InterfaceGetContactFailure {
	return {
		type: GET_CONTACT_FAILURE,
		data
	}
}

interface InterfaceGetContactRequest {
	type: string;
}

interface InterfaceGetContactSuccess {
	type: string;
	contact: ContactInterface;
}

interface InterfaceGetContactFailure {
	type: string;
	data: object;
}