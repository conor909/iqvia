import { API_REQUEST } from '../../store/middleware/api-middleware/_types';
import { ADD_CONTACT_REQUEST, ADD_CONTACT_SUCCESS, ADD_CONTACT_FAILURE } from '../_types';
import { ApiRequestInterface } from '../../store/middleware/api-middleware/index';

export default function addContact(contact: any): ApiRequestInterface {
	return {
		type: API_REQUEST,
		url: '/add-contact',
		data: contact,
		method: 'post',
		passToActions: {
			request: addContactRequest,
			success: addContactSuccess,
			failure: addContactFailure
		}
	}
}

export function addContactRequest(): InterfaceAddContactRequest {
	return {
		type: ADD_CONTACT_REQUEST
	}
}

export function addContactSuccess(data: Object): InterfaceAddContactSuccess {
	return {
		type: ADD_CONTACT_SUCCESS,
		contact: data
	}
}

export function addContactFailure(data: Object): InterfaceAddContactFailure {
	return {
		type: ADD_CONTACT_FAILURE,
		data
	}
}

interface InterfaceAddContactRequest {
	type: string;
}

interface InterfaceAddContactSuccess {
	type: string;
	contact: object;
}

interface InterfaceAddContactFailure {
	type: string;
	data: object;
}