import { API_REQUEST } from '../../store/middleware/api-middleware/_types';
import getContact, { getContactRequest, getContactSuccess, getContactFailure } from './get-contact';
import { GET_CONTACT_REQUEST, GET_CONTACT_SUCCESS, GET_CONTACT_FAILURE } from '../_types';

describe('Get Contact Actions', () => {
	it('should create an action to get a contact', () => {
		const contactId = 1;
		const action = getContact(contactId);
		expect(action).toMatchObject({
			type: API_REQUEST,
			url: '/contact',
			params: {
				id: contactId
			},
			method: 'get',
			passToActions: {
				request: getContactRequest,
				success: getContactSuccess,
				failure: getContactFailure
			}
		})
	})

	it('should create a request action', () => {
		const action = getContactRequest();
		expect(action).toMatchObject({
			type: GET_CONTACT_REQUEST
		});
	})

	it('should create a success action', () => {
		const action = getContactSuccess({ name: 'John Smith' });
		expect(action).toMatchObject({
			type: GET_CONTACT_SUCCESS,
			contact: { name: 'John Smith' }
		});
	})

	it('should create a failure action', () => {
		const action = getContactFailure();
		expect(action).toMatchObject({
			type: GET_CONTACT_FAILURE
		});
	})
});