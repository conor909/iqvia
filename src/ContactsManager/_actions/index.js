import addContact from './add-contact';
import getContacts from './get-contacts';
import getContact from './get-contact';
import deleteContact from './delete-contact';
import editContact from './edit-contact';

export {
	getContacts,
	getContact,
	addContact,
	deleteContact,
	editContact
}