import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { API_REQUEST } from '../../store/middleware/api-middleware/_types';
import deleteContact, { deleteContactRequest, deleteContactSuccess, deleteContactFailure } from './delete-contact';
import { DELETE_CONTACT_REQUEST, DELETE_CONTACT_SUCCESS, DELETE_CONTACT_FAILURE } from '../_types';

describe('Delete Contact Actions', () => {
	it('should create an action to delete a contact', () => {

		const contactId = 1;
		const action = deleteContact(contactId);
		expect(action).toMatchObject({
			type: API_REQUEST,
			url: '/delete-contact',
			data: { contactId: 1 },
			method: 'post',
			passToActions: {
				request: deleteContactRequest,
				success: deleteContactSuccess,
				failure: deleteContactFailure
			}
		})
	})

	it('should create a request action', () => {
		const action = deleteContactRequest();
		expect(action).toMatchObject({
			type: DELETE_CONTACT_REQUEST
		});
	});

	it('should dispatch a success action', () => {
		const mockStore = configureStore([thunk])
		const initialState = {
			contactsManager: {
				list: [
					{
						id: 'abc',
						name: 'John Smith'
					},
					{
						id: 'xyz',
						name: 'Mary McDonald'
					}
				]
			}
		}
		const store = mockStore(initialState);
		store.dispatch(deleteContactSuccess('xyz'))
		const actions = store.getActions()
		const expectedPayload = { type: DELETE_CONTACT_SUCCESS, contactIndex: 1 }
		expect(actions).toEqual([expectedPayload]);
	});

	it('should create a delete action', () => {
		const action = deleteContactRequest();
		expect(action).toMatchObject({
			type: DELETE_CONTACT_REQUEST
		});
	});

})