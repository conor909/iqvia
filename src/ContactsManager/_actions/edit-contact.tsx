import { API_REQUEST } from '../../store/middleware/api-middleware/_types';
import { EDIT_CONTACT_REQUEST, EDIT_CONTACT_SUCCESS, EDIT_CONTACT_FAILURE } from '../_types';
import { StoreInterface } from '../../store/Interfaces';
import { ApiRequestInterface } from '../../store/middleware/api-middleware/index';
import { ContactInterface } from '../Interfaces'

export default function editContact(contactUpdates: ContactUpdatesInterface): ApiRequestInterface {
	return {
		type: API_REQUEST,
		url: '/edit-contact',
		data: contactUpdates,
		method: 'post',
		passToActions: {
			request: editContactRequest,
			success: editContactSuccess,
			failure: editContactFailure
		}
	}
}

export function editContactRequest() {
	return {
		type: EDIT_CONTACT_REQUEST
	}
}

export function editContactSuccess(data: SuccessResponseInterface) {
	return (dispatch: any, getStore: () => StoreInterface) => {
		const targetId = data.id;
		dispatch({
			type: EDIT_CONTACT_SUCCESS,
			contactIndex: getStore().contactsManager.list.findIndex((contact: ContactInterface) => (contact.id === targetId)),
			updatedContact: data
		})
	}
}

export function editContactFailure(response: object) {
	return {
		type: EDIT_CONTACT_FAILURE,
		response
	}
}

export interface ContactUpdatesInterface {
	id: String;
	field: String;
	value: any
}

export interface SuccessResponseInterface {
	id: String;
}