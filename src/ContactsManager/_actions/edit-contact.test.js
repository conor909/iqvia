import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { API_REQUEST } from '../../store/middleware/api-middleware/_types';
import editContact, { editContactRequest, editContactSuccess, editContactFailure } from './edit-contact';
import { EDIT_CONTACT_REQUEST, EDIT_CONTACT_SUCCESS, EDIT_CONTACT_FAILURE } from '../_types';

describe('edit Contact Actions', () => {
	it('should create an action to edit a contact', () => {
		const contactUpdates = {
			id: 1,
			field: 'name',
			value: 'Johnathan'
		};
		const action = editContact(contactUpdates);
		expect(action).toMatchObject({
			type: API_REQUEST,
			url: '/edit-contact',
			data: contactUpdates,
			method: 'post',
			passToActions: {
				request: editContactRequest,
				success: editContactSuccess,
				failure: editContactFailure
			}
		})
	})

	it('should create a request action', () => {
		const action = editContactRequest();
		expect(action).toMatchObject({
			type: EDIT_CONTACT_REQUEST
		});
	})

	it('should create a success action', () => {
		const mockStore = configureStore([thunk])
		const initialState = {
			contactsManager: {
				list: [
					{
						id: 'abc',
						name: 'John Smith'
					},
					{
						id: 'xyz',
						name: 'Mary McDonald'
					}
				]
			}
		}
		const store = mockStore(initialState);
		store.dispatch(editContactSuccess({ id: 'xyz', name: 'Mary Jane McDonald' }))
		const actions = store.getActions()
		const expectedPayload = {
			type: EDIT_CONTACT_SUCCESS,
			contactIndex: 1,
			updatedContact: { id: 'xyz', name: 'Mary Jane McDonald' }
		}
		expect(actions).toEqual([expectedPayload]);
	})

	it('should create a failure action', () => {
		const action = editContactFailure();
		expect(action).toMatchObject({
			type: EDIT_CONTACT_FAILURE
		});
	})
});