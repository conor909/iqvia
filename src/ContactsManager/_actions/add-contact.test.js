import { API_REQUEST } from '../../store/middleware/api-middleware/_types';
import { ADD_CONTACT_REQUEST, ADD_CONTACT_SUCCESS, ADD_CONTACT_FAILURE } from '../_types';
import addContact, { addContactRequest, addContactSuccess, addContactFailure } from './add-contact';

describe('Add Contact Actions', () => {

	it('should create an add contact action', () => {
		const newContact = {
			name: 'John Smith',
			email: 'john@email.com'
		};
		const action = addContact(newContact);
		expect(action).toMatchObject({
			type: API_REQUEST,
			url: '/add-contact',
			data: newContact,
			method: 'post',
			passToActions: {
				request: addContactRequest,
				success: addContactSuccess,
				failure: addContactFailure
			}
		});
	});

	it('should create a request action', () => {
		const action = addContactRequest();
		expect(action).toMatchObject({
			type: ADD_CONTACT_REQUEST
		})
	});

	it('should create a success action', () => {
		const action = addContactSuccess();
		expect(action).toMatchObject({
			type: ADD_CONTACT_SUCCESS
		});
	});

	it('should create a success action', () => {
		const action = addContactFailure();
		expect(action).toMatchObject({
			type: ADD_CONTACT_FAILURE
		});
	});

});