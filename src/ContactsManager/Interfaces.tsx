export interface ContactInterface {
	id?: String;
	name?: String;
	email?: String;
	createdDate?: String;
	dateModified?: String;
}