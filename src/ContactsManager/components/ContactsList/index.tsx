import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { getContacts, getContact, deleteContact, editContact } from '../../_actions';
import { ContactUpdatesInterface } from '../../_actions/edit-contact';
import ContactsList from './List';
import { Loading } from '../../../common-components';
import { ContactInterface } from '../../../ContactsManager/Interfaces';
import { StoreInterface } from '../../../store/Interfaces';

const ContactListContainer = (props: ContactListContainerInterface) => {

	useEffect(() => {
		props.getContacts();
	}, [])

	return props.contactList
		? <ContactsList
			getContact={props.getContact}
			editContact={props.editContact}
			contactList={props.contactList}
			deleteContact={props.deleteContact} />
		: <Loading />

}

const mapStateToProps = (store: StoreInterface) => {
	// would use reselect for memoization
	return {
		contactList: store.contactsManager.list
	}
}

const dispatchToProps = (dispatch: any) => {
	return {
		editContact: (contactUpdates: ContactUpdatesInterface) => dispatch(editContact(contactUpdates)),
		getContacts: () => dispatch(getContacts()),
		getContact: (contactId: string) => dispatch(getContact(contactId)),
		deleteContact: (contactId: string) => dispatch(deleteContact(contactId))
	}
}

export default connect(mapStateToProps, dispatchToProps)(ContactListContainer);

export interface ContactListContainerInterface {
	contactList: Array<ContactInterface>;
	getContacts: () => void;
	getContact: (id: string) => void;
	editContact: (contactUpdates: ContactUpdatesInterface) => void;
	deleteContact: (id: string) => void;
}