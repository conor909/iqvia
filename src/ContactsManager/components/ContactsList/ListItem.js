import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import { TextField } from '../../../common-components';

export default (props) => {

	const [nameFieldText, setNameFieldText] = useState(props.name);
	const [emailFieldText, setEmailFieldText] = useState(props.email);

	function handleGetContact() {
		props.getContact(props.contactId)
	}

	function handleEditName(value) {
		props.editContact({
			id: props.contactId,
			field: 'name',
			value
		})
	}

	function handleEditEmail(value) {
		props.editContact({
			id: props.contactId,
			field: 'email',
			value
		})
	}

	function handleDeleteContact() {
		props.deleteContact(props.contactId)
	}

	const classes = useStyles();

	return (
		<div className={classes.root}>
			<ListItem>
				<ListItemAvatar>
					<div onClick={handleGetContact}>
						<Avatar>
							<VisibilityIcon />
						</Avatar>
					</div>
				</ListItemAvatar>
				<div className={classes.details}>
					<TextField
						value={nameFieldText}
						onChange={setNameFieldText}
						onBlur={handleEditName} />
					<TextField
						value={emailFieldText}
						onChange={setEmailFieldText}
						onBlur={handleEditEmail} />
					<div style={{ display: 'flex', flexDirection: 'column' }}>
						<span style={{ fontSize: '10px' }}>
							{`created date: ${props.createdDate}`}
						</span>
						{
							props.dateModified &&
							<span style={{ fontSize: '10px' }}>
								{`date modified: ${props.dateModified}`}
							</span>
						}
					</div>
				</div>
				<ListItemSecondaryAction>
					<div onClick={handleDeleteContact}>
						<IconButton edge="end" aria-label="Delete">
							<DeleteIcon />
						</IconButton>
					</div>
				</ListItemSecondaryAction>
			</ListItem>
		</div>
	)
}

const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1
	},
	details: {
		display: 'flex',
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	}
}));