import React from 'react';
import { List, Grid } from '@material-ui/core';
import ListItem from './ListItem';
import { ContactUpdatesInterface } from '../../_actions/edit-contact';
import { ContactInterface } from '../../Interfaces';

export default (props: ListInterface) => {

	function getContact(contactId: string) {
		props.getContact(contactId)
	}

	function deleteContact(contactId: string) {
		props.deleteContact(contactId)
	}

	function editContact(contactUpdates: ContactUpdatesInterface) {
		props.editContact(contactUpdates)
	}

	return (
		<div style={{ flexGrow: 1 }}>
			<Grid
				container
				direction="row"
				justify="center"
				alignItems="center"
				spacing={4}>
				<Grid item xs={6}>
					<List>
						{
							props.contactList
								.filter(contact => contact)
								.map((contact: ContactInterface) => {
									return (
										<ListItem
											key={contact.id}
											getContact={getContact}
											deleteContact={deleteContact}
											editContact={editContact}
											createdDate={contact.createdDate}
											dateModified={contact.dateModified}
											contactId={contact.id}
											name={contact.name}
											email={contact.email} />
									)
								})
						}
					</List>
				</Grid>
			</Grid>
		</div>
	)
};

interface ListInterface {
	contactList: Array<ContactInterface>;
	getContact: (id: string) => void;
	editContact: (contactUpdates: ContactUpdatesInterface) => void;
	deleteContact: (id: string) => void;
}