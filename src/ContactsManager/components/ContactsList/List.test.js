import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import List from './index';
import thunk from 'redux-thunk';
import apiMiddleware from '../../../store/middleware/api-middleware';

const middlewares = [thunk, apiMiddleware]
const mockStore = configureStore(middlewares);

describe('List Component', () => {
	let wrapper;
	const initialState = {
		contactsManager: {
			list: [
				{
					id: 'unique-key-1',
					name: 'John Smith',
					email: 'john@email.com'
				}
			]
		}
	};

	beforeEach(() => {
		wrapper = shallow(
			<Provider store={mockStore(initialState)}>
				<List />
			</Provider>
		)
	});

	it('renders without crashing', () => {
		const div = document.createElement('div');
		ReactDOM.render(wrapper, div);
		ReactDOM.unmountComponentAtNode(div);
	});
});