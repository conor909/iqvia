import React, { useState, useEffect } from 'react';
import { Dialog, DialogTitle, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import { connect } from 'react-redux';
import { ContactInterface } from '../../Interfaces';
import { StoreInterface } from '../../../store/Interfaces';

interface ContactsViewInterface {
	contact: ContactInterface
}

const ContactView = (props: ContactsViewInterface) => {

	const [isOpen, setIsOpen] = useState(false);

	useEffect(() => {
		if (!!props.contact.id) {
			setIsOpen(true)
		}
	}, [props.contact]);

	function handleClose() {
		setIsOpen(false)
	}

	return (
		isOpen
			? <Dialog open={true} onClose={handleClose} aria-labelledby="simple-dialog-title">
				<DialogTitle id="simple-dialog-title">Contact</DialogTitle>
				<Table>
					<TableHead>
						<TableRow>
							<TableCell>{'name'}</TableCell>
							<TableCell>{'email'}</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						<TableRow>
							<TableCell component="th" scope="row">
								{props.contact.name}
							</TableCell>
							<TableCell>{props.contact.email}</TableCell>
						</TableRow>
					</TableBody>
				</Table>
			</Dialog>
			: null
	)
}

const mapStateToProps = (store: StoreInterface) => {
	return {
		contact: store.contactsManager.view.contact
	}
}

export default connect(mapStateToProps)(ContactView);