import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import ContactsForm from './index';
import thunk from 'redux-thunk';
import apiMiddleware from '../../../store/middleware/api-middleware';
import uuid from 'uuid/v4';;

const middlewares = [thunk, apiMiddleware]
const mockStore = configureStore(middlewares);

describe('ContactsForm Component', () => {
	let wrapper;
	const mockAddContact = jest.fn();
	const initialState = {
		contactsManager: {
			list: [
				{
					id: uuid(),
					name: 'John Smith',
					email: 'john@email.com'
				}
			]
		}
	};

	beforeEach(() => {
		wrapper = shallow(
			<Provider store={mockStore(initialState)}>
				<ContactsForm addContact={mockAddContact} />
			</Provider>
		)
	});

	it('renders without crashing', () => {
		const div = document.createElement('div');
		ReactDOM.render(wrapper, div);
		ReactDOM.unmountComponentAtNode(div);
	});

});