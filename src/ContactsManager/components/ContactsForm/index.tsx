import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, TextField } from '../../../common-components';
import Grid from '@material-ui/core/Grid';
import { addContact } from '../../_actions';
import { ContactInterface } from '../../../ContactsManager/Interfaces';

interface ContactsFormInterface {
	addContact: (Object: ContactInterface) => void
}

const ContactsForm = (props: ContactsFormInterface) => {

	const [contactFieldText, setContactFieldText] = useState('');
	const [emailFieldText, setEmailFieldText] = useState('');

	function handleCreateNewContact() {
		props.addContact({
			name: contactFieldText,
			email: emailFieldText
		});
		setContactFieldText('');
		setEmailFieldText('');
	}

	return (
		<div style={{ flexGrow: 1 }}>
			<Grid
				container
				direction="column"
				justify="center"
				alignItems="center"
				spacing={4}>
				<Grid item xs={12}>
					<TextField
						id='contact-field'
						label='Full Name'
						value={contactFieldText}
						onChange={setContactFieldText} />
				</Grid>
				<Grid item xs={12}>
					<TextField
						id='email-field'
						label='Email'
						value={emailFieldText}
						onChange={setEmailFieldText} />
				</Grid>
				<Grid item xs={12}>
					<Button
						text='Add Contact'
						onClick={handleCreateNewContact} />
				</Grid>
			</Grid>
		</div>
	)
}

const mapStateToProps = (state: Object) => ({});

const dispatchToProps = (dispatch: any) => {
	return {
		addContact: (contact: ContactInterface) => dispatch(addContact(contact))
	}
}

export default connect(mapStateToProps, dispatchToProps)(ContactsForm);