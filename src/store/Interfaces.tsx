
import { ContactsManagerInterface } from '../ContactsManager/_reducers';

export interface StoreInterface {
	contactsManager: ContactsManagerInterface
}