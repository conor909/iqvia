import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './root-reducer';
import apiMiddleware from './middleware/api-middleware';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middleware = applyMiddleware(thunk, apiMiddleware);

export default function configureStore(initialState = {}) {
	return createStore(
		rootReducer,
		initialState,
		composeEnhancers(middleware)
	);
};