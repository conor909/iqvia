import axios, { AxiosResponse } from 'axios';
import { API_REQUEST } from './_types';

export default function apiMiddleware({ dispatch }: ApiMiddlewareInterface) {
	return (next: (action: any) => void) => (action: ApiRequestInterface) => {

		if (action.type !== API_REQUEST) {
			next(action);
			return;
		}

		const { request, success, failure } = action.passToActions;

		dispatch(request())

		axios.request({
			url: 'http://localhost:3001' + action.url,
			method: action.method,
			data: action.data || {},
			params: action.params || {}
		})
			.then((response: AxiosResponse) => {
				if (response.status === 200) {
					dispatch(success(response.data, action.passToReducers));
				} else {
					dispatch(failure(response.data, action.passToReducers));
				}
			});
	}
}

export interface ApiRequestInterface {
	type: string;
	url: string;
	data?: object;
	params?: object;
	method: 'post' | 'get';
	passToActions: PassToActionsInteface,
	passToReducers?: object
}

interface ApiMiddlewareInterface {
	dispatch: (action: any) => void;
	action: ApiRequestInterface;
}

interface PassToActionsInteface {
	request: () => void;
	success: (data: any, passToReducers: any) => void;
	failure: (data: any, passToReducers: any) => void;
}