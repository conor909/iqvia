import { combineReducers } from 'redux';
import contactReducers from '../ContactsManager/_reducers';

const rootReducer = combineReducers({
	contactsManager: contactReducers
});

export default rootReducer;