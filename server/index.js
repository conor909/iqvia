const express = require('express');
const bodyParser = require('body-parser');
const low = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');
const cors = require('cors');
const uuid = require('uuid/v4')
const moment = require('moment');

// Create server
const app = express()
app.use(bodyParser.json())
app.use(cors({ origin: ['http://localhost:3000'], credentials: true }));

// Create database instance and start server
const adapter = new FileAsync('server/db.json');
low(adapter)
	.then(db => {
		db.defaults({ contacts: [] }).write();

		// Routes
		// GET /contacts
		app.get('/contacts', (req, res) => {
			const contacts = db.get('contacts').value();
			res.send(contacts);
		})

		// GET /contact/:id
		app.get('/contact', (req, res) => {
			const contact = db.get('contacts')
				.find({ id: req.query.id })
				.value()
			res.send(contact)
		})

		// POST /add-contact
		app.post('/add-contact', (req, res) => {
			db.get('contacts')
				.push(req.body)
				.last()
				.assign({
					id: uuid(),
					createdDate: moment().format('LLLL'),
					dateModified: false
				})
				.write()
				.then(newContact => res.send(newContact))
		})

		// POST /delete-contact
		app.post('/delete-contact', (req, res) => {
			db.get('contacts')
				.remove({ id: req.body.contactId })
				.write()
				.then(() => res.send(req.body.contactId))
		})

		// POST /edit-contact
		app.post('/edit-contact', (req, res) => {
			const targetContact = db.get("contacts").find({ id: req.body.id }).value();
			const updatedContact = {
				...targetContact,
				[req.body.field]: req.body.value,
				dateModified: moment().format('LLLL')
			}
			db.get('contacts')
				.find({ id: targetContact.id })
				.assign(updatedContact)
				.write()
				.then(() => res.send(updatedContact))
		})

		return db
	})
	.then(() => {
		app.listen(3001, () => console.log('listening on port 3001'))
	})