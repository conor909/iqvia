## Available Scripts

To install packages use Yarn (contains a Yarn.lock file):

+ `yarn`

In the project directory, you can run 3 terminals:

+ `yarn start`  
+ `yarn run server`  
+ `yarn test`

## Code Files

Main code files are in:

+ `src/ContactsManager/`  
+ `src/store/`  
+ `server/index.js`  

## Typechecked features:

+ `src/store/api-middleware/`
+ `src/ContactsManager/components/`   
+ `src/ContactsManager/_actions/`

## Tested features:

+ `src/App/`
+ `src/ContactsManager/components/`   
+ `src/ContactsManager/_actions/`